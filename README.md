
# mapicons

A variety of icons suitable for use on maps.

This package builds a variety of icons suitable for use on maps.

PNG and SVGs are built/copied into the dist directory.

# NPM

This package is available on [npm](https://www.npmjs.com/):

[@lowswaplab/mapicons](https://www.npmjs.com/package/@lowswaplab/mapicons)

This package can be installed by performing:

    npm install @lowswaplab/mapicons

# Source Code

[mapicons](https://gitlab.com/lowswaplab/mapicons)

# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).

# Author

[Low SWaP Lab](https://www.lowswaplab.com/)

# Copyright

Copyright © 2021 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

